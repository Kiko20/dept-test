import React, { createContext, useEffect, useState } from "react";

export const Context = createContext({});

export const Provider = (props) => {
  const [toggle, setToggle] = useState(false);
  const [visible, setVisible] = useState(true);
  const [open, setOpen] = useState(false);
  const [emailVal, setEmailVal] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [success, setSuccess] = useState("");
  const [error, setError] = useState("");
  const [emailError, setEmailError] = useState(false);
  const [firstError, setFirstError] = useState(false);
  const [lastError, setLastError] = useState(false);
  const onOpenModal = () => setOpen(true);
  const onCloseModal = () => setOpen(false);
  const data = {
    name: firstName,
    last: lastName,
  };
  //TOGGLE PLAY OR PAUSE ON THE HERO VIDEO
  const pausePlay = () => {
    const video = document.getElementById("video");
    if (video.paused) {
      video.play();
    } else {
      video.pause();
    }
  };
  // EMAIL VALIDATION FUNCTION
  const ValidateEmail = (e) => {
    e.preventDefault();
    if (emailVal !== "" && firstName !== "" && lastName !== "") {
      fetch("https://api.mocki.io/v1/b043df5a", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
      })
        .then((response) => response.json())
        .then((data) => {
          console.log("Success:", data);
          setOpen(false);
          setEmailVal("");
          setFirstName("");
          setLastName("");
          setEmailError(false);
          setFirstError(false);
          setLastError(false);
          setSuccess("success");
          setError("");
        })
        .catch((error) => {
          console.error("Error:", error);
          setSuccess("");
        });
    } else {
      setSuccess("");
      setError("error");
      emailVal === "" && setEmailError(true);
      firstName === "" && setFirstError(true);
      lastName === "" && setLastError(true);
    }
  };

  //SEPARATE EMAIL VALIDATION
  const EmailValidate = (e, value) => {
    e.preventDefault();
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(value)) {
      setEmailVal(value);
    } else if (value.length < 1) {
      setEmailVal("");
    }
  };
  //SEPARATE FIRSTNAME VALIDATION
  const FirstNameValidate = (e, value) => {
    e.preventDefault();
    if (/^[a-z ,.'-]+$/.test(value.toLowerCase()) && value.length > 1) {
      setFirstName(value);
    } else if (value.length < 1) {
      setFirstName("");
    }
  };
  //SEPARATE LASTNAME VALIDATION
  const LastNameValidate = (e, value) => {
    e.preventDefault();
    if (/^[a-z ,.'-]+$/.test(value.toLowerCase()) && value.length > 1) {
      setLastName(value);
    } else if (value.length < 1) {
      setLastName("");
    }
  };
  //IF THE MODAL IS CLOSED WITHOUT ENTERING SOME OF THE VALUES, IT RESETS THE VALUES
  //IF REQUESTED,THE VALUES CAN BE STORED IN LOCAL STORAGE
  useEffect(() => {
    if (open === false) {
      setEmailVal("");
      setFirstName("");
      setLastName("");
    }
  }, [open]);

  return (
    <Context.Provider
      value={{
        pausePlay,
        toggle,
        setToggle,
        visible,
        setVisible,
        open,
        onCloseModal,
        onOpenModal,
        ValidateEmail,
        success,
        setSuccess,
        error,
        setError,
        emailVal,
        setEmailVal,
        firstName,
        setFirstName,
        lastName,
        setLastName,
        firstError,
        setFirstError,
        lastError,
        setLastError,
        emailError,
        setEmailError,
        EmailValidate,
        FirstNameValidate,
        LastNameValidate,
      }}
    >
      {props.children}
    </Context.Provider>
  );
};
