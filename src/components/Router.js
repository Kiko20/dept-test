import React from "react";
import Homepage from "./Homepage";

const Routes = {
  "/": () => <Homepage />,
};

export default Routes;
