import React from "react";
//NPM
import styled from "styled-components";
//IMAGES
import logo from "../Images/dept.svg";
import fb from "../Images/facebook.svg";
import twit from "../Images/twitter.svg";
import google from "../Images/google.svg";
//CSS STYLES
const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  background-color: black;
  padding: 6rem 1rem;
  @media (min-width: 320px) and (max-width: 600px) {
    padding: 1rem;
    flex-direction: column;
  }
`;
const Menus = styled.div`
  display: flex;
  justify-content: flex-start;
  width: 50%;
  align-items: flex-end;
  img {
    width: 20%;
    margin-right: 20px;
    @media (min-width: 320px) and (max-width: 768px) {
      display: none;
    }
  }
  @media (min-width: 320px) and (max-width: 600px) {
    flex-direction: column;
    align-items: flex-start;
  }
`;
const Social = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: flex-end;
  width: 50%;
  img {
    width: 25px;
    height: 25px;
    margin-right: 10px;
    cursor: pointer;
  }
  @media (min-width: 320px) and (max-width: 600px) {
    justify-content: flex-start;
  }
`;
const Btn = styled.button`
  color: white;
  font-size: 23px;
  background-color: transparent;
  border: none;
  font-weight: bold;
  line-height: 0.7;
  transition: 0.6s;
  cursor: pointer;
  &:hover {
    color: #ff5454;
    transition: 0.6s;
  }
  &:focus {
    outline: none;
  }
  @media (min-width: 320px) and (max-width: 768px) {
    font-size: 18px;
    text-align: left;
  }
  @media (min-width: 320px) and (max-width: 600px) {
    margin-bottom: 20px;
  }
`;

const Navigation = () => {
  return (
    <Wrapper>
      <Menus>
        <img src={logo}></img>
        <Btn>CASES </Btn>
        <Btn>OVER</Btn>
        <Btn>STORIES</Btn>
        <Btn>DIENSTEN</Btn>
      </Menus>
      <Social>
        <img src={fb}></img>
        <img src={twit}></img>
        <img src={google}></img>
      </Social>
    </Wrapper>
  );
};

export default Navigation;
