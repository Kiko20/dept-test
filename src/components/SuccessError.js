import React, { useContext, useEffect } from "react";
//NPM
import styled from "styled-components";
//COMPONENTS
import { Context } from "./Context";
//CSS STYLES
const Wrapper = styled.div`
  position: absolute;
  top: 90%;
  left: 50%;
  transform: translate(-50%, -50%);
  background-color: ${(props) =>
    props.opacity === "success" ? "green" : "red"};
  color: white;
  padding: 10px;
  border-radius: 5px;
  opacity: ${(props) =>
    props.opacity === "success" ? "1" : props.opacity === "error" ? "1" : 0};
`;

const SuccessError = (props) => {
  const { success, setSuccess, error, setError } = useContext(Context);
  //AFTER 2 SECONDS IT HIDES THE MESSAGE THAT WE GET AFTER ENTERING OUR INFORMATION
  useEffect(() => {
    const timer = setTimeout(() => {
      setSuccess("");
    }, 2000);
  }, [success]);
  useEffect(() => {
    const timer = setTimeout(() => {
      setError("");
    }, 2000);
  }, [error]);
  return (
    <>
      <Wrapper opacity={success || error}>
        <p>{props.message}</p>
      </Wrapper>
    </>
  );
};

export default SuccessError;
