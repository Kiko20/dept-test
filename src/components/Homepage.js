import React, { useEffect, useState, useContext } from "react";
//NPM
import styled from "styled-components";
import "react-responsive-modal/styles.css";
import { Modal } from "react-responsive-modal";
//COMPONENTS
import { Context } from "./Context";
import Form from "./Form";
import SuccessError from "./SuccessError";
import Navigation from "./Navigation";
//IMAGES
import play from "../Images/play.svg";
import pause from "../Images/pause.svg";
import logo from "../Images/dept.svg";
import logoBlack from "../Images/dept-black.svg";

//CSS STYLES
const MainWrapper = styled.div`
  display: flex;
  flex-direction: column;
`;
const Wrapper = styled.div`
  position: relative;
  .visible {
    opacity: 0;
    transition: 0.6s;
  }
  height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  @media (min-width: 320px) and (max-width: 768px) {
    height: unset;
  }
  /* @media (min-width: 1100px) and (max-width: 1200px) {
    height: 70vh;
    max-height: 1080px;
  }
  @media (min-width: 1200px) and (max-width: 1300px) {
    height: 75vh;
    max-height: 1080px;
  }
  @media (min-width: 1300px) and (max-width: 1440px) {
    height: 80vh;
    max-height: 1080px;
  }
  @media (min-width: 1440px) {
    height: 100vh;
    max-height: 1080px;
  } */
  /* @media (min-width: 1600px) {
    height: 100vh;
    max-height: 1080px;
  } */
`;
const Video = styled.video`
  width: 100%;
  top: 0;
  left: 0;
  padding: none;
  position: absolute;
  z-index: -1;
  object-fit: fill;
  height: 100%;
  @media (min-width: 320px) and (max-width: 768px) {
    z-index: 0;
    position: relative;
    object-fit: cover;
    height: unset;
  }
  @media (min-width: 1920px) {
    object-fit: cover;
  }
`;
const Btn = styled.button`
  position: absolute;
  top: 52%;
  left: 50%;
  transform: translate(-50%, -50%);
  border-radius: 50%;
  width: 73px;
  height: 73px;
  border: none;
  &:focus {
    outline: none;
  }
  &:hover {
    opacity: 1;
  }
  background-color: white;
  opacity: 1;
  transition: 0.6s;
  @media (min-width: 320px) and (max-width: 768px) {
    top: 63%;
  }
`;
const Img = styled.img`
  background-color: white;
  border-radius: 50%;
  width: 55%;
`;
const LogoCont = styled.div`
  display: flex;
  justify-content: flex-start;
  padding: 35px 0px 25px 0px;
  margin: 0px 30px;
  border-bottom: 1px solid white;
  img {
    width: 6.5%;
    color: white;
    &:last-of-type {
      display: none;
      @media (min-width: 320px) and (max-width: 768px) {
        width: 30%;
        display: block;
      }
    }
    &:first-of-type {
      @media (min-width: 320px) and (max-width: 768px) {
        width: 30%;
        display: none;
      }
    }
  }
`;
const BottomWrapper = styled.div`
  display: ${(props) => (props.desk ? "flex" : "none")};
  justify-content: space-between;
  margin-bottom: 30px;
  padding: 40px;
  @media (min-width: 320px) and (max-width: 480px) {
    flex-direction: column;
    padding: 20px;
  }
  @media (min-width: 320px) and (max-width: 768px) {
    display: ${(props) => (props.desk ? "none" : "flex")};
  }
  div {
    display: flex;
    flex-direction: column;
    &:last-of-type {
      justify-content: flex-end;
    }
    h1 {
      color: white;
      text-align: left;
      font-size: 32px;
      @media (min-width: 320px) and (max-width: 768px) {
        font-size: 21px;
        color: black;
      }
      @media (min-width: 480px) and (max-width: 768px) {
        font-size: 22px;
      }
      @media (min-width: 769px) and (max-width: 1200px) {
        margin-top: 100px;
      }
    }
    p {
      color: white;
      text-align: left;
      font-size: 1rem;
      @media (min-width: 320px) and (max-width: 768px) {
        color: black;
      }
    }
    button {
      color: white;
      background-color: black;
      border: none;
      padding: 10px 20px 10px 20px;
      font-size: 16px;
      @media (min-width: 320px) and (max-width: 768px) {
        width: 200px;
        padding: 10px;
        font-size: 14px;
      }
      @media (min-width: 550px) and (max-width: 800px) {
        padding: 15px;
        font-size: 12px;
      }
      &:focus {
        outline: none;
      }
      margin-right: 10px;
      transition: 0.3s;
      &::after {
        content: "►";
        display: inline-block;
        padding-left: 10px;
        transition: 0.3s;
      }
      &:hover {
        &:after {
          padding-left: 25px;
        }
        padding-right: 10px;
      }
    }
  }
`;

const Homepage = () => {
  const {
    pausePlay,
    toggle,
    visible,
    setToggle,
    setVisible,
    open,
    onOpenModal,
    onCloseModal,
    success,
  } = useContext(Context);
  //AFTER 5 SECONDS IT HIDES THE PLAY/PAUSE BUTTON ON THE VIDEO
  useEffect(() => {
    const timer = setTimeout(() => {
      setVisible(false);
    }, 5000);
  }, [visible]);
  //IF THE MODAL IS CLOSED, IT STARTS THE VIDEO AND VICE VERSA.
  useEffect(() => {
    const video = document.getElementById("video");
    if (open) {
      video.pause();
    } else {
      video.play();
    }
  }, [open]);
  return (
    <MainWrapper>
      <Wrapper>
        <LogoCont>
          <img src={logo}></img>
          <img src={logoBlack}></img>
        </LogoCont>
        <Video
          autoPlay
          muted
          loop
          id="video"
          onMouseOver={() => setVisible(true)}
        >
          <source
            src="https://player.vimeo.com/external/344108443.hd.mp4?s=c1730857e5c3097471ce4d0e05b3bfd287ab561f&profile_id=175"
            type="video/mp4"
          />
        </Video>
        <Btn
          className={visible ? "" : "visible"}
          onClick={() => {
            pausePlay();
            setToggle(!toggle);
          }}
          onMouseEnter={() => setVisible(true)}
        >
          {toggle ? <Img src={play}></Img> : <Img src={pause}></Img>}
        </Btn>

        <BottomWrapper desk>
          <div>
            <h1>
              We are going to create:<br></br> A modal with a newsletter form!!
            </h1>
            <p>Let's get started!</p>
          </div>
          <div>
            <button onClick={onOpenModal}>Subscribe to newsletter</button>
          </div>
        </BottomWrapper>
        <Modal open={open} onClose={onCloseModal} center>
          <Form />
        </Modal>
        <SuccessError
          message={
            success === "success"
              ? "Bedankt voor uw aanmelding!🎉"
              : "Please enter valid information"
          }
        />
      </Wrapper>
      <BottomWrapper>
        <div>
          <h1>
            We are going to create:<br></br> A modal with a newsletter form!!
          </h1>
          <p>Let's get started!</p>
        </div>
        <div>
          <button onClick={onOpenModal}>Subscribe to newsletter</button>
        </div>
      </BottomWrapper>
      <Navigation />
    </MainWrapper>
  );
};

export default Homepage;
