import React, { useContext, useEffect, useState } from "react";

//NPM
import styled from "styled-components";

//COMPONENTS
import { Context } from "./Context";

//CSS STYLES
const NewForm = styled.form`
  display: flex;
  flex-direction: column;
  input {
    padding: 10px;
  }
  .red {
    border: 1px solid red;
  }
  h2 {
    @media (min-width: 320px) and (max-width: 768px) {
      font-size: 18px;
    }
  }
  #submit {
    color: white;
    background-color: black;
    border: none;
    padding: 10px 20px 10px 20px;
    font-size: 16px;
    border-radius: 3px;
    width: 45%;
    @media (min-width: 320px) and (max-width: 768px) {
      width: 40%;
      padding: 10px;
      font-size: 14px;
    }
    @media (min-width: 550px) and (max-width: 800px) {
      padding: 15px;
      font-size: 12px;
    }
    &:focus {
      outline: none;
    }
  }
`;
const NamesDiv = styled.div`
  padding: 30px 0px;
  display: flex;
  justify-content: space-between;
  input {
    width: 45%;
    border-radius: 3px;
    @media (min-width: 320px) and (max-width: 768px) {
      width: 40%;
    }
  }
`;
const BtnCont = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
`;

const Form = () => {
  const [show, setShow] = useState(false);
  const {
    ValidateEmail,
    success,
    setSuccess,
    error,
    setError,
    emailVal,
    setEmailVal,
    firstName,
    setFirstName,
    lastName,
    setLastName,
    firstError,
    setFirstError,
    lastError,
    setLastError,
    emailError,
    setEmailError,
    EmailValidate,
    FirstNameValidate,
    LastNameValidate,
  } = useContext(Context);
  //IF VALID EMAIL IS ENTERED, THE FIRSTNAME AND LASTNAME INPUTS ARE SHOWN
  useEffect(() => {
    emailVal.length > 0 ? setShow(true) : setShow(false);
  }, [emailVal]);

  return (
    <NewForm onSubmit={(e) => ValidateEmail(e, emailVal, firstName, lastName)}>
      <h2>Niks willen missen van onze nieuwtjes en acties?</h2>
      <input
        placeholder="Email Address"
        onChange={(e) => EmailValidate(e, e.target.value)}
        className={emailError ? "red" : ""}
      ></input>
      {show && (
        <>
          <NamesDiv>
            <input
              type="text"
              placeholder="First name"
              onChange={(e) => FirstNameValidate(e, e.target.value)}
              className={firstError ? "red" : ""}
            ></input>
            <input
              type="text"
              placeholder="Last name"
              onChange={(e) => LastNameValidate(e, e.target.value)}
              className={lastError ? "red" : ""}
            ></input>
          </NamesDiv>
          <BtnCont>
            <input type="submit" id="submit"></input>
          </BtnCont>
        </>
      )}
    </NewForm>
  );
};

export default Form;
