import logo from "./logo.svg";
import "./App.css";
import { Provider } from "./components/Context";
import Routes from "./components/Router";
import { useRoutes } from "hookrouter";

function App() {
  const routes = useRoutes(Routes);
  return (
    <div className="App">
      <Provider>{routes}</Provider>
    </div>
  );
}

export default App;
